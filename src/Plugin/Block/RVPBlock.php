<?php

namespace Drupal\uvrp\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'Ubercart Recently Viewed Products' Block.
 *
 * @Block(
 *   id = "uvrp",
 *   admin_label = @Translation("Ubercart Recently Viewed Products"),
 * )
 */
class RVPBlock extends BlockBase implements BlockPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();
    $global_config = \Drupal::config('urvp.settings');
    $limit = $global_config->get('uvrp_limit');

    $form['uvrp_limit'] = [
      '#type' => 'textfield',
      '#attributes' => [
        ' type' => 'number',
      ],
      '#title' => $this->t('Number of products to show'),
      '#default_value' => $config['uvrp_limit'] ?? $limit ?? 4,
      '#description' => $this->t("Specify the number of products to show."),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $this->configuration['uvrp_limit'] = $form_state->getValue('uvrp_limit');
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state) {
    if ($form_state->getValue('uvrp_limit') <= 0) {
      $form_state->setErrorByName('uvrp_limit', $this->t('Enter only positive integers.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $config = $this->getConfiguration();
    $limit = $config['uvrp_limit'];
    $form['#markup'] = uvrp_block_display($limit);
    $form['#cache']['max-age'] = 0;
    return $form;

  }

}
