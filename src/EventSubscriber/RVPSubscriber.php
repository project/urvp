<?php

namespace Drupal\uvrp\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Drupal\node\NodeInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Configure uvrp Subscriber for this site.
 */
class RVPSubscriber implements EventSubscriberInterface {

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Constructs an event subscriber.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(RequestStack $request_stack) {
    $this->requestStack = $request_stack;
  }

  /**
   * Redirect pattern based url.
   *
   * @param \Symfony\Component\HttpKernel\Event\GetResponseEvent $event
   *   The response event, which contains the current request.
   */
  public function rvp(GetResponseEvent $event) {
    $session = $this->uvrpSession();
    if (!$session) {
      $session_id    = $this->drupalHashBase64();
      $cookie_domain = ini_get('session.cookie_domain');
      setcookie('uvrp', $session_id, \Drupal::time()->getRequestTime() + 2592000, '/', $cookie_domain, 1, 1);
    }

    $node = uvrp_get_current_node();
    if ($node instanceof NodeInterface) {
      if ($node->getType() == 'product') {
        $uvrp_cookie = $this->requestStack->getCurrentRequest()->cookies->get('uvrp');
        \Drupal::database()->merge('uvrp')->key([
          'nid' => $node->id(),
          'sid' => $uvrp_cookie ?? session_id(),
          'uid' => uvrp_current_user_id(),
        ])->fields([
          'ip' => uvrp_client_ip(),
          'created' => \Drupal::time()->getRequestTime(),
        ])->execute();
      }
    }
  }

  /**
   * Hash the data value.
   *
   * @return string
   *   The hash base64 string value.
   */
  private function drupalHashBase64() {
    $uniqid = uniqid(mt_rand(), TRUE);
    $hash = base64_encode(hash('sha256', $uniqid, TRUE));
    // Modify the hash so it's safe to use in URLs.
    return strtr($hash, ['+' => '-', '/' => '_', '=' => '']);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      KernelEvents::REQUEST => 'rvp',
    ];
  }

  /**
   * Check if cookie variable is set.
   */
  private function uvrpSession() {
    $uvrp_cookie = $this->requestStack->getCurrentRequest()->cookies->get('uvrp');
    if (isset($uvrp_cookie) && !empty($uvrp_cookie)) {
      return TRUE;
    }
    else {
      $this->requestStack->getCurrentRequest()->cookies->set('uvrp', '');
      return FALSE;
    }
  }

}
